# -*- coding: utf-8 -*-
"""
Created on Mon Dec 14 23:45:44 2015

@author: ilija
"""

#Primjer 4.1.
import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error

def non_func(x):
        y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
        return y
        
def add_noise(y):
        np.random.seed(14)
        varNoise = np.max(y) - np.min(y)
        y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
        return y_noisy
#2.zadatak        
def zatvorena_forma(xtrain,ytrain):
        k = np.c_[np.ones(len(xtrain)),xtrain] 
        u = np.linalg.inv(np.dot(np.transpose(k),k))
        l = np.dot(np.transpose(k),ytrain)
        thetaML = np.dot(u,l)
        return thetaML
#gradijentni postupak(3.zadatak)       
def costfunction(X, ytrain, theta):
        m = float(X.shape[0])
        cost = (1./(2.*m))*(X*theta-ytrain).T*(X*theta-ytrain)
        return cost.flat[0]

def gradient(X, ytrain, theta, iter, alpha):
        theta_iter = [] #record theta for each iteration
        cost_iter = []  #record cost for each iteration
        m = float(X.shape[0])

        for i in range(iter):
            #update theta
            theta = theta-(alpha/m)*X.T*(X*theta-ytrain)
            theta_iter.append(theta)
            cost_iter.append(costfunction(X,ytrain,theta))

        return(theta, theta_iter, cost_iter)
 ######################################         
x = np.linspace(1,10,100)
y_true = non_func(x)
y_measured = add_noise(y_true)

plt.figure(1)
plt.plot(x,y_measured,'ok',label='mjereno')
plt.plot(x,y_true,label='stvarno')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 4)

np.random.seed(12)
indeksi = np.random.permutation(len(x))
indeksi_train = indeksi[0:int(np.floor(0.7*len(x)))]
indeksi_test = indeksi[int(np.floor(0.7*len(x)))+1:len(x)]

x = x[:, np.newaxis]
y_measured = y_measured[:, np.newaxis]

xtrain = x[indeksi_train]
ytrain = y_measured[indeksi_train]

xtest = x[indeksi_test]
ytest = y_measured[indeksi_test]

plt.figure(2)
plt.plot(xtrain,ytrain,'ob',label='train')
plt.plot(xtest,ytest,'or',label='test')
plt.xlabel('x')
plt.ylabel('y')
plt.legend(loc = 4)

linearModel = lm.LinearRegression()
linearModel.fit(xtrain,ytrain)

print 'Model je oblika y_hat = Theta0 + Theta1 * x'
print 'y_hat = ', linearModel.intercept_, '+', linearModel.coef_, '*x'

ytest_p = linearModel.predict(xtest)
MSE_test = mean_squared_error(ytest, ytest_p)

plt.figure(3)
plt.plot(xtest,ytest_p,'og',label='predicted')
plt.plot(xtest,ytest,'or',label='test')
plt.legend(loc = 4)

x_pravac = np.array([1,10])
x_pravac = x_pravac[:, np.newaxis]
y_pravac = linearModel.predict(x_pravac)
plt.plot(x_pravac, y_pravac)

a=zatvorena_forma(xtrain, ytrain)
print a

###########################
#3.zadatak
#load data
x=np.c_[np.ones(len(xtrain)),xtrain]
theta = np.matrix([[0.],[0.]])
alpha = 0.003
iter = 3000
#gradient descent
theta, theta_iter, cost_iter = gradient(x, ytrain, theta, iter, alpha)

rezultat = x*theta
plt.figure(4)
plt.plot(xtrain, rezultat)
plt.scatter(xtrain, ytrain, marker='x', c='r')
plt.xlabel('x')
plt.ylabel('y')
plt.grid(True)
plt.xlim((1,10))
plt.ylim((-2,5))
plt.show()

plt.figure(5)
plt.plot(np.arange(0, iter, 1), cost_iter)
plt.xlabel('Iteration ')
plt.ylabel('MSE- (eng.mean squared error)')
plt.show()

